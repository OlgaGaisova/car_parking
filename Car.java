package car_parking;

public class Car {
    private int carNumber;
    private int numberOfMoves;

    public int getCarNumber() {
        return carNumber;
    }
    public int getNumberOfMoves() {
        return numberOfMoves;
    }

    public void setCarNumber(int carNumber) {
        this.carNumber = carNumber;
    }
    public void setNumberOfMoves(int numberOfMoves) {
        this.numberOfMoves = numberOfMoves;
    }
}
